## Synopsis

Code used for the STM32 on Logic Analyzer adapter board for Rigol MSO5000 oscilloscope.
STM32 is not required for operation, only used for displaying pod/cable status, and trigger voltage levels.


## Hardware

STM32F030 IC
SSD1306 OLED (128x32)
USB-C to USB-C cable, must be passive type 5 or 10Gbps

## PCB
- Rev_1: some issues, few hardware mods needed but will work, extra PCB available.
- Rev_2: see folder for details.

## License

MIT license where applicable.  