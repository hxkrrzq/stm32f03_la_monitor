## Revision notes:
Rev 1:
- Original with swapped diff pairs that needs a few hardware mods

Rev 2:
- Fix differential pair swaps
- Add ADC protection resistors, for negative trigger voltage (will be clipped)
- Add 2.5V to ADC pin
- Add CC2 resistor (may be optional)


## PCB manufacture requirements (JLCPCB):

- Upload gerber, 4 layer will be detected.
- Choose dimensions: 72x65mm
- PCB thickness 1.6mm (will have to bend the header connector slightly down when soldering)
- Impedance -> Yes, PCB material: JLC7628 (controlled impedance)
- PCB color: of your choosing
- Copper: 1oz
- Panel: no
- Different design: 2
Price: ~$43



For 100 ohm differential:
space 5mil
trace: 6mil
https://jlcpcb.com/client/index.html#/impedance