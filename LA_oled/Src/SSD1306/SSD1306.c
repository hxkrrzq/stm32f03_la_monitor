/**
  ******************************************************************************
  * @file    SSD1306.c 
  * @author  Waveshare Team / thomas
  * @version 
  * @date    13-October-2014
  * @brief   This file includes the OLED driver for SSD1306 display moudle
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, WAVESHARE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LIB_Config.h"
#include "SSD1306.h"
#include "Fonts.h"
#include "stdlib.h"
#include "stdbool.h"
#include "string.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SSD1306_CMD    0
#define SSD1306_DAT    1

#define SSD1306_WIDTH    128
#define SSD1306_HEIGHT   32


/* Private macro -------------------------------------------------------------*/

#if !defined(SH1106) && !defined(SSD1306)
	#warning Please select first the target OLED device(SH1106 or SSD1306) in your application!
	#define SSD1306  //define SSD1306 by default	
#endif

#if defined(SSD1306)
#define __SET_COL_START_ADDR() 	do { \
									ssd1306_write_byte(0x00, SSD1306_CMD); \
									ssd1306_write_byte(0x10, SSD1306_CMD); \
								} while(false)
#elif defined(SH1106)
#define __SET_COL_START_ADDR() 	do { \
									ssd1306_write_byte(0x02, SSD1306_CMD); \
									ssd1306_write_byte(0x10, SSD1306_CMD); \
								} while(false)
#endif								
/* Private variables ---------------------------------------------------------*/
static uint8_t s_chDispalyBuffer[128][8];

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Writes an byte to the display data ram or the command register
  *         
  * @param  chData: Data to be writen to the display data ram or the command register
  * @param chCmd:  
  *                           0: Writes to the command register
  *                           1: Writes to the display data ram
  * @retval None
**/
static void ssd1306_write_byte(uint8_t chData, uint8_t chCmd) 
{
#ifdef INTERFACE_4WIRE_SPI
	
	__SSD1306_CS_CLR();
	
	if (chCmd) {
		__SSD1306_DC_SET();
	} else {
		__SSD1306_DC_CLR();
	}	
	__SSD1306_WRITE_BYTE(chData);
	
	__SSD1306_DC_SET();
	__SSD1306_CS_SET();
	
#elif defined(INTERFACE_3WIRE_SPI)
	
	uint8_t i;
	uint16_t hwData = 0;
	
	if (chCmd) {
		hwData = (uint16_t)chData | 0x0100;
	} else {
		hwData = (uint16_t)chData & ~0x0100;
	}
	
	__SSD1306_CS_CLR();
	for(i = 0; i < 9; i ++) {
		__SSD1306_CLK_CLR();
		
		if(hwData & 0x0100) {
			__SSD1306_DIN_SET();
		} else {
			__SSD1306_DIN_CLR();
		}
		__SSD1306_CLK_SET();
		hwData <<= 1;
	}
	__SSD1306_CS_SET();
	
#elif defined(INTERFACE_IIC)
	/*
	uint8_t cmd[3] = { 0x40, 0x00 };
	if (chCmd)
	{
		HAL_I2C_Master_Transmit(SSD1306_I2C_PORT, SSD1306_I2C_ADDR, cmd, 1, 10); 
		HAL_I2C_Master_Transmit(SSD1306_I2C_PORT, SSD1306_I2C_ADDR, &chData, 1, 10); 
	}
	
	else
	{
		HAL_I2C_Master_Transmit(SSD1306_I2C_PORT, SSD1306_I2C_ADDR, &cmd[1], 1, 10); 
		HAL_I2C_Master_Transmit(SSD1306_I2C_PORT, SSD1306_I2C_ADDR, &chData, 1, 10); 		
	}*/
	
	iic_start();
	iic_write_byte(0x78);
	iic_wait_for_ack();
	if (chCmd) {
		iic_write_byte(0x40);
		iic_wait_for_ack();
	} else {
		iic_write_byte(0x00);
		iic_wait_for_ack();
	}
	iic_write_byte(chData);
	iic_wait_for_ack();
	
	iic_stop();
	
#endif
}   	  

/**
  * @brief  OLED turns on 
  *         
  * @param  None
  *         
  * @retval None
**/ 
void ssd1306_display_on(void)
{
	ssd1306_write_byte(SSD1306_CHARGEPUMP, SSD1306_CMD);		//charge pump setting
	ssd1306_write_byte(SSD1306_EN_CHARGEPUMP, SSD1306_CMD);		//enable charge pump
	ssd1306_write_byte(SSD1306_DISPLAYON, SSD1306_CMD);  
}
   
/**
  * @brief  OLED turns off
  *         
  * @param  None
  *         
  * @retval  None
**/
void ssd1306_display_off(void)
{
	ssd1306_write_byte(SSD1306_CHARGEPUMP, SSD1306_CMD);  //charge pump setting
	ssd1306_write_byte(SSD1306_DIS_CHARGEPUMP, SSD1306_CMD);	
	ssd1306_write_byte(SSD1306_DISPLAYOFF, SSD1306_CMD);  
}

/**
  * @brief  Refreshs the graphic ram
  *         
  * @param  None
  *         
  * @retval  None
**/

void ssd1306_update(void)
{
	uint8_t i, j;
	
	for (i = 0; i < 8; i ++) {  
		ssd1306_write_byte(0xB0 + i, SSD1306_CMD);    
		__SET_COL_START_ADDR();      
		for (j = 0; j < 128; j ++) {
			ssd1306_write_byte(s_chDispalyBuffer[j][i], SSD1306_DAT); 
		}
	}   
}


/**
  * @brief   Clears the screen
  *         
  * @param  None
  *         
  * @retval  None
**/

void ssd1306_clear_screen(uint8_t chFill)  
{ 
	uint8_t i, j;
	
	for (i = 0; i < 8; i ++) {
		ssd1306_write_byte(0xB0 + i, SSD1306_CMD);
		__SET_COL_START_ADDR();
		for (j = 0; j < 128; j ++) {
			s_chDispalyBuffer[j][i] = chFill;
		}
	}
	
	//ssd1306_update();
}

/**
  * @brief  Horizontally flip screen (mirror)
  * @param  None
  * @retval  None
**/

void ssd1306_flip_screen(bool flip)
{
	if (flip)
		ssd1306_write_cmd(0xC8);
	else
	{
		ssd1306_write_cmd(0xC0);
	}
}

void ssd1306_rotate_screen(bool rot)
{
	if (rot)
	{
		ssd1306_write_cmd(0xA0);
		ssd1306_write_cmd(0xC8);	
	}
	else
	{
		ssd1306_write_cmd(0xA1);
		ssd1306_write_cmd(0xC0);	
	}
}


/**
  * @brief   Invert colors
  *         
  * @param  None
  *         
  * @retval  None
**/

void ssd1306_invert_screen(bool invert)
{
	if (invert)
		ssd1306_write_cmd(SSD1306_INVERTDISPLAY);
	else
	{
		ssd1306_write_cmd(SSD1306_NORMALDISPLAY);
	}
}

//Set the brightness of the screen

void ssd1306_set_contrast(uint8_t contrast)
{
	ssd1306_write_cmd(SSD1306_SETCONTRAST);
	ssd1306_write_cmd(contrast);		//default is 207
}


//from mbed https://developer.mbed.org/users/Byrn/code/SSD1306/docs/1d9df877c90a/ssd1306_8cpp_source.html

void ssd1306_start_horizontal_scroll(uint8_t direction, uint8_t start, uint8_t end, SSD1306_FREQ interval) 
{
	ssd1306_write_cmd(direction ? 0x27 : 0x26);
	ssd1306_write_cmd(0x00);
	ssd1306_write_cmd(start & 0x07);

	ssd1306_write_cmd((uint8_t) interval);
	ssd1306_write_cmd(end & 0x07);
	ssd1306_write_cmd(0x00);
	ssd1306_write_cmd(0xFF);
    
	// activate scroll
	ssd1306_write_cmd(0x2F);
}
 
void ssd1306_start_vertical_and_horizontal_scroll(uint8_t direction, uint8_t start, uint8_t end, SSD1306_FREQ interval, uint8_t vertical_offset)
{
	ssd1306_write_cmd(direction ? 0x2A : 0x29);
	ssd1306_write_cmd(0x00);
	ssd1306_write_cmd(start & 0x07);
	ssd1306_write_cmd((uint8_t) interval);
	/*
	switch (interval) {
	case FREQ_2:	ssd1306_write_cmd(0x07); break; // 111b
	case FREQ_3:	ssd1306_write_cmd(0x04); break; // 100b
	case FREQ_4:	ssd1306_write_cmd(0x05); break; // 101b
	case FREQ_5:	ssd1306_write_cmd(0x00); break; // 000b
	case FREQ_25:	ssd1306_write_cmd(0x06); break; // 110b
	case FREQ_64:	ssd1306_write_cmd(0x01); break; // 001b
	case FREQ_128:	ssd1306_write_cmd(0x02); break; // 010b
	case FREQ_256:	ssd1306_write_cmd(0x03); break; // 011b
	default:
	    // default to 2 frame interval
		ssd1306_write_cmd(0x07); break;
	}*/
	ssd1306_write_cmd(end & 0x07);
	ssd1306_write_cmd(vertical_offset);    
    
	// activate scroll
	ssd1306_write_cmd(0x2F);
}
 
void ssd1306_stop_scroll()
{
    // all scroll configurations are removed from the display when executing this command.
	ssd1306_write_cmd(0x2E);
}



/**
  * @brief  Draws a piont on the screen
  *         
  * @param  chXpos: Specifies the X position
  * @param  chYpos: Specifies the Y position
  * @param  chPoint: 0: the point turns off    1: the piont turns on 
  *         
  * @retval None
**/

void ssd1306_draw_point(uint8_t chXpos, uint8_t chYpos, uint8_t chPoint)
{
	uint8_t chPos, chBx, chTemp = 0;
	
	if (chXpos > 127 || chYpos > 63) {
		return;
	}
	chPos = 7 - chYpos / 8; // 
	chBx = chYpos % 8;
	chTemp = 1 << (7 - chBx);
	
	if (chPoint) {
		s_chDispalyBuffer[chXpos][chPos] |= chTemp;
		
	} else {
		s_chDispalyBuffer[chXpos][chPos] &= ~chTemp;
	}
}
	  

/**
  * @brief  Draws a line
  *         
  * @param  chXpos: Specifies the X position
  * @param  chYpos: Specifies the Y position
  * @param  chPoint: 0: the point turns off    1: the piont turns on 
  *         
  * @retval None
**/

void ssd1306_draw_line(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t chPoint)
{
	int i, deltax, deltay, numpixels;
	int d, dinc1, dinc2;
	int x, xinc1, xinc2;
	int y, yinc1, yinc2;
	//calculate deltaX and deltaY
	deltax = abs(x2 - x1);
	deltay = abs(y2 - y1);
	//initialize
	if(deltax >= deltay)
	{
	//If x is independent variable
		numpixels = deltax + 1;
		d = (2 * deltay) - deltax;
		dinc1 = deltay << 1;
		dinc2 = (deltay - deltax) << 1;
		xinc1 = 1;
		xinc2 = 1;
		yinc1 = 0;
		yinc2 = 1;
	}
	else
	{
	//if y is independent variable
		numpixels = deltay + 1;
		d = (2 * deltax) - deltay;
		dinc1 = deltax << 1;
		dinc2 = (deltax - deltay) << 1;
		xinc1 = 0;
		xinc2 = 1;
		yinc1 = 1;
		yinc2 = 1;
	}
	//move the right direction
	if(x1 > x2)
	{
		xinc1 = -xinc1;
		xinc2 = -xinc2;
	}
	if(y1 > y2)
	{
		yinc1 = -yinc1;
		yinc2 = -yinc2;
	}
	x = x1;
	y = y1;
	//draw the pixels
	for(i = 1; i < numpixels; i++)
	{
		ssd1306_draw_point(x, y, chPoint);
		if(d < 0)
		{d = d + dinc1;
			x = x + xinc1;
			y = y + yinc1;
		}
		else
		{
			d = d + dinc2;
			x = x + xinc2;
			y = y + yinc2;
		}
	}
}


/**
  * @brief  Fills a rectangle
  *         
  * @param  chXpos1: Specifies the X position 1 (X top left position)
  * @param  chYpos1: Specifies the Y position 1 (Y top left position)
  * @param  chXpos2: Specifies the X position 2 (X bottom right position)
  * @param  chYpos3: Specifies the Y position 2 (Y bottom right position)
  *         
  * @retval 
**/

void ssd1306_fill_screen(uint8_t chXpos1, uint8_t chYpos1, uint8_t chXpos2, uint8_t chYpos2, uint8_t chDot)  
{  
	uint8_t chXpos, chYpos; 
	
	for (chXpos = chXpos1; chXpos <= chXpos2; chXpos ++) {
		for (chYpos = chYpos1; chYpos <= chYpos2; chYpos ++) {
			ssd1306_draw_point(chXpos, chYpos, chDot);
		}
	}	
	
	//ssd1306_update();
}



void ssd1306_display_char_sm(uint8_t chXpos, uint8_t chYpos, uint8_t chChr, FONT_SIZE chSize, uint8_t chMode)
{      	
	uint8_t i, j;
	uint8_t chTemp = 0, chYpos0 = chYpos;
	
	chChr = chChr - ' ';				   
	for (i = 0; i < chSize; i++) {   
		switch (chSize)
		{
		case FONT_V_SMALL:
			chTemp = c_chFont0806[chChr][i];
			break;
		case FONT_MED:
			chTemp = c_chFont1206[chChr][i];
			break;
		case FONT_LARGE:
			//chTemp = c_chFont1608[chChr][i];
			break;
		default: break;
		}
	
		if (!chMode)
			chTemp = ~chTemp;
		
		for (j = 0; j < 8; j++) {
			//ssd1306_draw_point(chXpos, chYpos, (bool)(chTemp & 0x80));
			if (chTemp & 0x80) {
				ssd1306_draw_point(chXpos, chYpos, 1);
			}
			else {
				ssd1306_draw_point(chXpos, chYpos, 0);
			}
			chTemp <<= 1;
			chYpos++;
			
			if ((chYpos - chYpos0) == chSize) 
			{
				chYpos = chYpos0;
				chXpos++;
				break;
			}
		}  	 
	} 
}


static uint32_t pow_(uint8_t m, uint8_t n)
{
	uint32_t result = 1;	 
	while(n --) result *= m;    
	return result;
}


void ssd1306_display_num(uint8_t chXpos, uint8_t chYpos, uint32_t chNum, uint8_t chLen, uint8_t chSize)
{         	
	uint8_t i;
	uint8_t chTemp, chShow = 0;
	
	for(i = 0; i < chLen; i ++) {
		chTemp = (chNum / pow_(10, chLen - i - 1)) % 10;
		if(chShow == 0 && i < (chLen - 1)) {
			if(chTemp == 0) {
				ssd1306_display_char(chXpos + (chSize / 2) * i, chYpos, ' ', chSize, 1);
				continue;
			} else {
				chShow = 1;
			}	 
		}
	 	ssd1306_display_char(chXpos + (chSize / 2) * i, chYpos, chTemp + '0', chSize, 1); 
	}
} 


/**
  * @brief  Writes a string on the screen buffer
  *         
  * @param  chXpos: Specifies the X position
  * @param  chYpos: Specifies the Y position
  * @param  pchString: Pointer to a string to display on the screen 
  *         
  * @retval  None
**/
void ssd1306_display_string(uint8_t x, uint8_t y, const uint8_t *pchString, uint8_t chSize, uint8_t chMode)	//remove const string
{
    while (*pchString != '\0') {       
        if (x > (SSD1306_WIDTH - chSize / 2)) {		//wrap line
			x = 0;
			y += chSize;
			if (y > (SSD1306_HEIGHT - chSize)) {		//clear display and start at 0 if past end
				y = x = 0;
				ssd1306_clear_screen(0x00);
			}
		}
		
        //ssd1306_display_char(x, y, *pchString, chSize, chMode);
	    ssd1306_display_char_sm(x, y, *pchString, chSize, chMode);
        x += chSize / 2;
        pchString ++;
    }
}

/**
  * @brief  Displays a string on the screen
  *         
  * @param  chXpos: Specifies the X position
  * @param  chYpos: Specifies the Y position
  * @param  pchString: Pointer to a string to display on the screen 
  *         
  * @retval  None
**/
void ssd1306_display_string_up(uint8_t x, uint8_t y, const uint8_t *pchString, uint8_t chSize, uint8_t chMode)	//remove const string
{
	while (*pchString != '\0') {       
		if (x > (SSD1306_WIDTH - chSize / 2)) {		//wrap line
			x = 0;
			y += chSize;
			if (y > (SSD1306_HEIGHT - chSize)) {		//clear display and start at 0 if past end
				y = x = 0;
				ssd1306_clear_screen(0x00);
			}
		}
		
        //ssd1306_display_char(x, y, *pchString, chSize, chMode);
		ssd1306_display_char_sm(x, y, *pchString, chSize, chMode);
		x += chSize / 2;
		pchString++;
	}
	ssd1306_update();
}

/*
void ssd1306_draw_1616char(uint8_t chXpos, uint8_t chYpos, uint8_t chChar)
{
	uint8_t i, j;
	uint8_t chTemp = 0, chYpos0 = chYpos, chMode = 0;

	for (i = 0; i < 32; i ++) {
		chTemp = c_chFont1612[chChar - 0x30][i];
		for (j = 0; j < 8; j ++) {
			chMode = chTemp & 0x80? 1 : 0; 
			ssd1306_draw_point(chXpos, chYpos, chMode);
			chTemp <<= 1;
			chYpos ++;
			if ((chYpos - chYpos0) == 16) {
				chYpos = chYpos0;
				chXpos ++;
				break;
			}
		}
	}
}*/
/*
void ssd1306_draw_3216char(uint8_t chXpos, uint8_t chYpos, uint8_t chChar)
{
	uint8_t i, j;
	uint8_t chTemp = 0, chYpos0 = chYpos, chMode = 0;

	for (i = 0; i < 64; i ++) {
	
		if (chChar == 0x20)								//if character is space
			chTemp = c_chFont3216[11][i];
		else
			chTemp = c_chFont3216[chChar - 0x30][i];	//chars from 0 to 9
		
		for (j = 0; j < 8; j ++) {
			chMode = chTemp & 0x80? 1 : 0; 
			ssd1306_draw_point(chXpos, chYpos, chMode);
			chTemp <<= 1;
			chYpos ++;
			if ((chYpos - chYpos0) == 32) {
				chYpos = chYpos0;
				chXpos ++;
				break;
			}
		}
	}
}*/


void ssd1306_draw_3216str(uint8_t chXpos, uint8_t chYpos, uint8_t* str)
{
	uint8_t i = 0;
	
	while (str[i] != '\0')
	{
		ssd1306_draw_3216char(chXpos + i*16, chYpos, str[i]);
		i++;
	}
}


void ssd1306_draw_bitmap(uint8_t chXpos, uint8_t chYpos, const uint8_t *pchBmp, uint8_t chWidth, uint8_t chHeight)
{
	uint16_t i, j, byteWidth = (chWidth + 7) / 8;
	
    for(j = 0; j < chHeight; j ++){
        for(i = 0; i < chWidth; i ++ ) {
            if(*(pchBmp + j * byteWidth + i / 8) & (128 >> (i & 7))) {
                ssd1306_draw_point(chXpos + i, chYpos + j, 1);
            }
        }
    }
}


/**
  * @brief  SSd1306 initialization
  *         
  * @param  None
  *         
  * @retval None
**/
void ssd1306_init(void)
{

#ifdef INTERFACE_4WIRE_SPI	  
	__SSD1306_CS_SET();   //CS set
	__SSD1306_DC_CLR();   //D/C reset
	__SSD1306_RES_SET();  //RES set

#elif defined(INTERFACE_3WIRE_SPI)	
	__SSD1306_CS_CLR();   //CS reset
	__SSD1306_DC_CLR();   //D/C reset
	__SSD1306_RES_SET();  //RES set
	
#elif defined(INTERFACE_IIC)	  
	__IIC_SDA_SET();
	__IIC_SCL_SET();

#endif

	ssd1306_write_byte(0xAE, SSD1306_CMD);//--turn off oled panel
	ssd1306_write_byte(0x00, SSD1306_CMD);//---set low column address
	ssd1306_write_byte(0x10, SSD1306_CMD);//---set high column address
	ssd1306_write_byte(0x40, SSD1306_CMD);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
	
	ssd1306_write_byte(SSD1306_SETCONTRAST, SSD1306_CMD); //--set contrast control register
	ssd1306_write_byte(0xCF, SSD1306_CMD);// Set SEG Output Current Brightness
	
	ssd1306_write_byte(SSD1306_SEGREMAP, SSD1306_CMD);	//--Set SEG/Column Mapping. 0xA1 to mirror display
	ssd1306_write_byte(SSD1306_COMSCANDEC, SSD1306_CMD); //Set COM/Row Scan Direction   
	
	ssd1306_write_byte(0xA6, SSD1306_CMD);//--set normal display
	ssd1306_write_byte(0xA8, SSD1306_CMD);//--set multiplex ratio(1 to 64)
	ssd1306_write_byte(0x3f, SSD1306_CMD);//--1/64 duty
	ssd1306_write_byte(0xD3, SSD1306_CMD);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
	ssd1306_write_byte(0x00, SSD1306_CMD);//-not offset
	
	ssd1306_write_byte(SSD1306_SETDISPLAYCLOCKDIV, SSD1306_CMD);//--set display clock divide ratio/oscillator frequency
	ssd1306_write_byte(0x80, SSD1306_CMD);//--set divide ratio, Set Clock as 100 Frames/Sec
	
	ssd1306_write_byte(0xD9, SSD1306_CMD);//--set pre-charge period
	ssd1306_write_byte(0xF1, SSD1306_CMD);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	
	ssd1306_write_byte(0xDA, SSD1306_CMD);	//--set com pins hardware configuration
	ssd1306_write_byte(0x02, SSD1306_CMD);	//disable remap
	
	ssd1306_write_byte(0xDB, SSD1306_CMD);	//--set vcomh
	ssd1306_write_byte(0x40, SSD1306_CMD);	//Set VCOM Deselect Level
	ssd1306_write_byte(SSD1306_MEMORYMODE, SSD1306_CMD);	//-Set Page Addressing Mode (0x00/0x01/0x02)
	ssd1306_write_byte(0x02, SSD1306_CMD);	//
	
	ssd1306_write_byte(SSD1306_CHARGEPUMP, SSD1306_CMD);	//--set Charge Pump enable/disable
	ssd1306_write_byte(0x14, SSD1306_CMD);	//--set(0x10) disable
	
	ssd1306_write_byte(SSD1306_DISPLAYALLON_RESUME, SSD1306_CMD);	// Disable Entire Display On (0xa4/0xa5)
	ssd1306_write_byte(SSD1306_NORMALDISPLAY, SSD1306_CMD);	// Disable Inverse Display On (0xa6/a7) 
	ssd1306_write_byte(SSD1306_DISPLAYON, SSD1306_CMD);	//--turn on oled panel
	
	ssd1306_clear_screen(0x00);
}

/*-------------------------------END OF FILE-------------------------------*/

