

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once
#ifdef __cplusplus
extern "C" {
#endif
	 
/* Includes ------------------------------------------------------------------*/
#include "LIB_Config.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
	
#define CHK_MARK (95 + 32)
#define CON_WRONG (96 + 32)
#define A_TEST	(97 + 32)

	typedef enum
	{
		FONT_V_SMALL = 10,
		//FONT_SMALL = 9,
		FONT_MED     = 12,
		FONT_LARGE   = 16,
	}FONT_SIZE;

	/* Exported macro ------------------------------------------------------------*/
	/* Exported functions ------------------------------------------------------- */

	extern const uint8_t c_chFont0806[95][8];
	extern const uint8_t c_chFont1206[95+5][12];
	//extern const uint8_t c_chFont1608[95][16];
	//extern const uint8_t c_chFont1612[11][32];


	//icons
	extern const uint8_t icon_up_arrow[6];
	extern const uint8_t icon_down_arrow[6];
	
	//images
	extern const uint8_t icon_splash_screen[];

	#ifdef __cplusplus
}
#endif
/*-------------------------------END OF FILE-------------------------------*/

