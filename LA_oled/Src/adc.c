/**
  ******************************************************************************
  * File Name          : ADC.c
  * Description        : This file provides code for the configuration
  *                      of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/* USER CODE BEGIN 0 */
uint32_t adc_buff[NUM_ADC_CHANNELS] = { 0 };
uint32_t adc[NUM_ADC_AVGS][NUM_ADC_CHANNELS];

int get_average(adc_ch adc_channel);
uint16_t vrefint_cal;                            // VREFINT calibration value
float vdda = 0;
int adc_complete = 0;

/* USER CODE END 0 */

ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

/* ADC init function */
void MX_ADC_Init(void)
{
  ADC_ChannelConfTypeDef sConfig = {0};

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = ENABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_2;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_3;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_4;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_6;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspInit 0 */

  /* USER CODE END ADC1_MspInit 0 */
    /* ADC1 clock enable */
    __HAL_RCC_ADC1_CLK_ENABLE();
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**ADC GPIO Configuration    
    PA0     ------> ADC_IN0
    PA1     ------> ADC_IN1
    PA2     ------> ADC_IN2
    PA3     ------> ADC_IN3
    PA4     ------> ADC_IN4
    PA5     ------> ADC_IN5
    PA6     ------> ADC_IN6 
    */
    GPIO_InitStruct.Pin = ADC_VREF0_Pin|ADC_VREF1_Pin|ADC_CC0_Pin|ADC_CC1_Pin 
                          |ADC_CC2_Pin|ADC_CC3_Pin|ADC_2V5_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* ADC1 DMA Init */
    /* ADC Init */
    hdma_adc.Instance = DMA1_Channel1;
    hdma_adc.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_adc.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_adc.Init.MemInc = DMA_MINC_ENABLE;
    hdma_adc.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hdma_adc.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
    hdma_adc.Init.Mode = DMA_NORMAL;
    hdma_adc.Init.Priority = DMA_PRIORITY_MEDIUM;
    if (HAL_DMA_Init(&hdma_adc) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(adcHandle,DMA_Handle,hdma_adc);

  /* USER CODE BEGIN ADC1_MspInit 1 */
	  
		if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)		//Calibrate ADC (must be before startup)
			{
				Error_Handler();
			}
	  
  /* USER CODE END ADC1_MspInit 1 */
  }
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspDeInit 0 */

  /* USER CODE END ADC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_ADC1_CLK_DISABLE();
  
    /**ADC GPIO Configuration    
    PA0     ------> ADC_IN0
    PA1     ------> ADC_IN1
    PA2     ------> ADC_IN2
    PA3     ------> ADC_IN3
    PA4     ------> ADC_IN4
    PA5     ------> ADC_IN5
    PA6     ------> ADC_IN6 
    */
    HAL_GPIO_DeInit(GPIOA, ADC_VREF0_Pin|ADC_VREF1_Pin|ADC_CC0_Pin|ADC_CC1_Pin 
                          |ADC_CC2_Pin|ADC_CC3_Pin|ADC_2V5_Pin);

    /* ADC1 DMA DeInit */
    HAL_DMA_DeInit(adcHandle->DMA_Handle);
  /* USER CODE BEGIN ADC1_MspDeInit 1 */

  /* USER CODE END ADC1_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */

//******************************************************************************
void update_adc(void)
{
	uint32_t i;

	//sConfig.SamplingTime = ADC_SAMPLETIME_71CYCLES_5;   			//1/10MHz * cyles = //Rain max ~50k

	for(i = 0 ; i < NUM_ADC_AVGS ; i++)
	{
		HAL_ADC_Start_DMA(&hadc, &adc[i][0], NUM_ADC_CHANNELS);

		while(adc_complete == 0)
		{
		}
		
		HAL_ADC_Stop_DMA(&hadc);
		adc_complete = 0;
		HAL_Delay(1);
	}
	
}


//******************************************************************************
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	adc_complete = 1;
}

//******************************************************************************
int adc_current_val(adc_ch adc_channel)
{
	return get_average(adc_channel);
}

//******************************************************************************
int get_average(adc_ch adc_channel)
{
	uint32_t avg = 0;
	
	for (int i = 0; i < ARR_LEN(adc); i++)
	{
		avg += adc[i][adc_channel];	
	}
	
	return (avg / NUM_ADC_AVGS);     //return average
}


//******************************************************************************
float get_voltage(adc_ch i_adc)
{	
	int adc_avg = get_average(i_adc);
	
	//need to be scaled based on internal reference voltage
	//VDDA = 3.3V (or whatever) x vrefint cal / vrefint data
	float volts = (vdda / ADC_FULL_SCALE) * adc_avg;
	
	return volts;
}


//******************************************************************************
float get_temperature(void)
{
	int v_temp = get_average(ADC_TEMP);
	int vref = get_average(ADC_INT_REF);
	
	float temperature = (v_temp * (*VREFINT_CAL_ADDR)) / vref - (*TEMP30_CAL_ADDR);
	temperature *= 110 - 30;
	temperature /= (*TEMP110_CAL_ADDR - *TEMP30_CAL_ADDR);
	temperature += 30;
	
	return temperature;
}

//******************************************************************************
void cal_vref_int(void)
{
	//injected current 5mA max
	vrefint_cal = (*VREFINT_CAL_ADDR);       // read VREFINT_CAL_ADDR memory location
	
	//need 4us when measuring vrefint
	//1/40MHz = 
	int vrefint_data = get_average(ADC_INT_REF);	//adc_current_val(ADC_CHANNEL_VREFINT);	// read vrefint adc channel
	
	vdda = (3.3 * vrefint_cal) / vrefint_data;		//Calculate Vdda current voltage
}

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc)
{
	int test = 0;
	test++;
}
	

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
