/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ADC_VREF0_Pin GPIO_PIN_0
#define ADC_VREF0_GPIO_Port GPIOA
#define ADC_VREF1_Pin GPIO_PIN_1
#define ADC_VREF1_GPIO_Port GPIOA
#define ADC_CC0_Pin GPIO_PIN_2
#define ADC_CC0_GPIO_Port GPIOA
#define ADC_CC1_Pin GPIO_PIN_3
#define ADC_CC1_GPIO_Port GPIOA
#define ADC_CC2_Pin GPIO_PIN_4
#define ADC_CC2_GPIO_Port GPIOA
#define ADC_CC3_Pin GPIO_PIN_5
#define ADC_CC3_GPIO_Port GPIOA
#define ADC_2V5_Pin GPIO_PIN_6
#define ADC_2V5_GPIO_Port GPIOA
#define LED_Pin GPIO_PIN_1
#define LED_GPIO_Port GPIOB
#define SCL_Pin GPIO_PIN_9
#define SCL_GPIO_Port GPIOA
#define SDA_Pin GPIO_PIN_10
#define SDA_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

#define green_led_state() HAL_GPIO_ReadPin(LED_GPIO_Port, LED_Pin)
#define ARR_LEN(x) (sizeof(x)/sizeof(*x))

#define NUM_CABLES 4

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
